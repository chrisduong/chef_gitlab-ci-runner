name 'gitlab-ci-runner'
maintainer 'Sam4Mobile'
maintainer_email 'dps.team@s4m.io'
license 'Apache 2.0'
description 'Installs/Configures Gitlab CI Runner'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/s4m-chef-repositories/gitlab-ci-runner'
issues_url 'https://gitlab.com/s4m-chef-repositories/gitlab-ci-runner/issues'
version '1.3.0'

supports 'centos', '>= 7.1'
supports 'debian', '>= 8'

depends 'yum'
