#
# Copyright (c) 2015-2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

url = node['gitlab-ci-runner']['repository_url']
base = node['gitlab-ci-runner']['repository_base_url']

case node['platform_family']
when 'rhel'
  url ||= "#{base}/el/#{node['platform_version'].split('.').first}/$basearch"
  include_recipe 'yum'
  yum_repository 'gitlab-ci-runner' do
    description 'GitLab CI Runner'
    baseurl url
    gpgkey node['gitlab-ci-runner']['gpg_key']
    gpgcheck false
  end
when 'debian'
  package 'apt-transport-https'
  package 'lsb-release' do
    action :nothing
  end.run_action(:install)

  cmd = Mixlib::ShellOut.new('lsb_release -c -s')
  cmd.run_command
  codename = node['lsb']['codename'] || cmd.stdout.chomp

  url ||= "#{base}/#{node['platform']}"
  apt_repository 'gitlab-ci-runner' do
    uri url
    distribution codename
    components ['main']
    key node['gitlab-ci-runner']['gpg_key']
  end
end
